#+title: Moonlander XKB Layout
#+PROPERTY: header-args :mkdirp yes :comments link
#+auto_tangle: t

Ever since I grew up as a little child, I dreamed about once writing my own custom ~XKB~ configuration from scratch ...

I am using the [[https://programmer-dvorak.appspot.com/][Programmer Dvorak]] layout and have set up my *ZSA Moonlander* accordingly.  You can find my custom layout [[https://configure.zsa.io/moonlander/layouts/V6lVM/latest/0][here]].  Since I have just gotten my Moonlander yesterday, this is likely going to change a bit.

However, I've done some of the training modules on Windows and really like it so far.

This custom setup is for OpenBSD - you may not need this for Linux or FreeBSD.

* General Considerations

When using the /Programmer Dvorak/ layout, several of the regular keys in the top row map to symbols that only exist in shifted state on a normal keyboard.

For the BIOS to understand these mappings, the *Moonlander* needs to send an artificial shift key.  For instance, on a normal ~QWERTY~ keyboard, the second key from the top left is labeled ="1 / !"= - you get the exclamation mark by pressing the =1= key with =Shift= being pressed.  If you want to move that exclamation mark somewhere else and make it a regular, non-shifted key - while working fine from the BIOS - the *Moonlander* essentially tells the BIOS that this new key is =Shift-1=.

The downside to this is that the Operating System will have no way of distinguishing the regular key from it being used together with the =Shift= key, it will be the exact same keycode.

There are two ways how we can handle this with the *Moonlander*.

The most natural way - which I have chosen - is to bind the ~when tapped~ action to the key's normal state and the ~when held~ action to it's shifted state.  This has the big advantage that the *Moonlander* will then take care of the key mapping - so you will have full access to your layout from the BIOS and OS Boot Loader.

Another approach, that I tested earlier, is to bind the key to an "unused" regular key - either something like =F21= or an alphabetic key from a Non-Latin alphabet that exists in both lower- and upper-case.  You can then remap these in you Operating System - for instance using ~xkb~ - and the keys will work with and without =Shift=.  The downside to this is that you will not be able to use any of those keys in Windows without writing a custom scan code map - and they will not work at all at the Boot Loader.

I am quickly falling in love with the *Moonlander*'s auto-shift functionality and will likely ditch those =Shift= keys altogether.

_Update_:

As it turns out, using ~when held~ for capitalization works great for symbol keys, but isn't really such a good idea for Emacs and Vim users - as it makes it impossible to long-press a key to repeat it.  Also, binding any ~double click~ action might be a problem on certain Doom Emacs key combos - such as for instance =Spc g g= for =maggit-mode=.

Since I am a Software Engineer and not a Writer and also use *Doom Emacs* as my main GUI, I am going to use capital letters far less frequently than sequences of the same letter - I need to be able to hold a key to repeat it or press it multiple times as part of a control sequence.  So for all of the alphabet keys, I am now using ~when tapped, then held~ to toggle their upper-case version, and don't assign anything to ~when held~ and ~when double-tapped~.

My main ~Control~ key - located in the middle of the left column, where =Caps Lock= is (I never use ~Caps Lock~ and had bound that key to ~Control~ for ages) - is now a ~one-shot~, with a regular ~Control~ at the bottom of the left column.

We also need to bind ~when held~ to the key itself if we want support repetition while holding.

Prior to this last reboot, I had put this into my =/usr/X11R6/share/X11/xorg.conf.d/80-keyboard.conf= and then disconnected my old keyboard.  Now running *Moonlander* exclusively.

* Setup

See [[https://emacs.stackexchange.com/a/76664/40301][my post on StackExchange]] for details.

I define a custom ~xkb~ minor mode for this:

#+begin_src emacs-lisp
(define-minor-mode xkb-mode
  "Minor mode for editing XKB config files."
  :lighter nil
  (if xkb-mode
      (progn
        (setq-local comment-use-syntax nil)
        (setq-local comment-style 'indent)
        (setq-local comment-start "// "))))
#+end_src

* New Emacs Major Mode.

See [[org:../../../Workspace/moonlander-config/xkb-mode.org][xkb-mode.org]] for details.

* Makefile

Adding a standard GNU =Makefile= for update and installation.  I am using this on OpenBSD, the path names might need to be adjusted on other systems.

#+begin_src makefile :tangle Makefile
# Using GNU Make Syntax
DESTDIR			= /usr/X11R6/share/X11
XKB_DESTDIR		= $(DESTDIR)/xkb
CONF_DESTDIR	= $(DESTDIR)/xorg.conf.d
#+end_src

List all the files that we need to install.

#+begin_src makefile :tangle Makefile

XKB_FILES		= rules/moonlander rules/moonlander.lst \
				  keycodes/moonlander symbols/moonlander

CONF_FILES		= 80-keyboard.conf
#+end_src

Providing these variables here to allow overriding from the command-line.

#+begin_src makefile :tangle Makefile
DIFF			= diff
DOAS			= doas
INSTALL			= install
INSTALL_ARGS	= -m 0644 -o root -g bin
#+end_src

This is GNU Make Syntax.  Provide a helper to run a command for each element in a list.

#+begin_src makefile :tangle Makefile
.PHONY:			diff install update

define execute-command
$(1)

endef
#+end_src

Provide a ~diff~ target to easily check what has changed.

#+begin_src makefile :tangle Makefile
diff::
	$(foreach x,$(CONF_FILES),$(call execute-command,$(DIFF) -du $(CONF_DESTDIR)/$(x) $(x)))
	$(foreach x,$(XKB_FILES),$(call execute-command,$(DIFF) -du $(XKB_DESTDIR)/$(x) $(x)))
#+end_src

Install all the files and by default set their owner to ~root~ / ~bin~.  This requires ~doas~ and will prompt for your password repeatedly.

#+begin_src makefile :tangle Makefile
install::
	$(foreach x,$(CONF_FILES),$(call execute-command,$(DOAS) $(INSTALL) $(INSTALL_ARGS) $(x) $(CONF_DESTDIR)/$(x)))
	$(foreach x,$(XKB_FILES),$(call execute-command,$(DOAS) $(INSTALL) $(INSTALL_ARGS) $(x) $(XKB_DESTDIR)/$(x)))
#+end_src

Replace current keyboard layout with the new, uninstalled files, for quick testing.

#+begin_src makefile :tangle Makefile
setup::
	setxkbmap -I $(CURDIR) -rules moonlander -model moonlander -option "" -verbose 10
#+end_src
* X11 Setup

I have the following in =/usr/X11R6/share/X11/xorg.conf.d/80-keyboard.conf=:

#+begin_src text :tangle 80-keyboard.conf :comments no
Section "InputClass"
	Identifier	"system-keyboard"
	MatchIsKeyboard	"on"
	Option		"XkbRules"	"moonlander"
	Option		"XkdModel"	"moonlander"
EndSection
#+end_src

* Rules

Most basic ~rules~ files, using the standard layout, symbols and geometry.

#+begin_src xkb :tangle rules/moonlander
! model         = keycodes
  moonlander    = moonlander

! model         = layout
  moonlander    = moonlander

! layout        = keycodes
  moonlander    = moonlander

! model         = geometry
  moonlander    = pc(pc104)

! model         = symbols
  moonlander    = moonlander
#+end_src

We also need to create a most basic =moonlander.lst= file for =setxkbmap= to find our custom rules.

#+begin_src xkb :tangle rules/moonlander.lst
! model
  moonlander          KSA Moonlander Mark I
#+end_src

* File Headers

We are going to switch back and forth between the =keycodes/moonlander= and =symbols/moonlander= files to group things together, so write both file headers first.

#+begin_src xkb :tangle keycodes/moonlander :shebang "// -*- xkb -*-"
xkb_keycodes "moonlander" {
#+end_src

It looks like the =minimum= and =maximum= entries are required to not segfault the X server with the Shift key.

#+begin_src xkb :tangle keycodes/moonlander
    minimum         = 8;
    maximum         = 255;
#+end_src

Symbols file header.

#+begin_src xkb :tangle symbols/moonlander :shebang "// -*- xkb -*-"
default alphanumeric_keys modifier_keys

xkb_symbols "moonlander" {
    name[Group1] = "Moonlander";
#+end_src

* Keycodes

** Control keys.

#+begin_src xkb :tangle keycodes/moonlander
    <ESC>           = 9;
    <BKSP>          = 22;
    <TAB>           = 23;
    <RTRN>          = 36;
    <SPCE>          = 65;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <ESC>           { [  Escape      ] };
    key  <BKSP>          { [  BackSpace   ] };
    key  <TAB>           { [  Tab         ] };
    key  <RTRN>          { [  Return      ] };
    key  <SPCE>          { [  space       ] };
#+end_src

** Modifier keys.

*** Shift keys.

#+begin_src xkb :tangle keycodes/moonlander
    <LFSH>          = 50;
    <RTSH>          = 62;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <LFSH>          { [  Shift_L    ] };
    key  <RTSH>          { [  Shift_R    ] };

    modifier_map    Shift   { Shift_L, Shift_R };
#+end_src

*** Control

We only have a single ~Control~ key.

#+begin_src xkb :tangle keycodes/moonlander
    <LCTL>          = 37;
    <LALT>          = 64;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <LCTL>          { [  Control_L   ] };
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    modifier_map Control { Control_L };
#+end_src

*** Alt

We have two ~Alt~ keys, let's make them distinguishable.

#+begin_src xkb :tangle keycodes/moonlander
    <LALT>          = 64;
    <RALT>          = 113;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <LALT>          { [  Alt_L       ] };
    key  <RALT>          { [  Alt_R       ] };
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    modifier_map Mod1 { Alt_L };
    modifier_map Mod2 { Alt_R };
#+end_src

*** Super

~Super~ is generally expected to be in ~Mod4~ and we have a left and right version, so let's make them distinguishable.

#+begin_src xkb :tangle symbols/moonlander
    modifier_map Mod4 { Super_L };
    modifier_map Mod5 { Super_R };
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <LWIN>          { [  Super_L     ] };
    key  <RWIN>          { [  Super_R     ] };
#+end_src

#+begin_src xkb :tangle keycodes/moonlander
    <LWIN>          = 115;
    <RWIN>          = 116;
#+end_src

** Alphabet keys

Home row, left hand.

#+begin_src xkb :tangle keycodes/moonlander
    <AC01>          = 38;
    <AC02>          = 32;
    <AC03>          = 26;
    <AC04>          = 30;
    <AC05>          = 31;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <AC01>          { [  a,     A       ] };
    key  <AC02>          { [  o,     O       ] };
    key  <AC03>          { [  e,     E       ] };
    key  <AC04>          { [  u,     U       ] };
    key  <AC05>          { [  i,     I       ] };
#+end_src

Home row, right hand.

#+begin_src xkb :tangle keycodes/moonlander
    <AC06>          = 40;
    <AC07>          = 43;
    <AC08>          = 28;
    <AC09>          = 57;
    <AC10>          = 39;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <AC06>          { [  d,     D       ] };
    key  <AC07>          { [  h,     H       ] };
    key  <AC08>          { [  t,     T       ] };
    key  <AC09>          { [  n,     N       ] };
    key  <AC10>          { [  s,     S       ] };
#+end_src

Upper row, left hand.

#+begin_src xkb :tangle keycodes/moonlander
    <AD01>          = 47;
    <AD02>          = 59;
    <AD03>          = 60;
    <AD04>          = 33;
    <AD05>          = 29;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <AD01>          { [  semicolon,  colon       ] };
    key  <AD02>          { [  comma,      less        ] };
    key  <AD03>          { [  period,     greater     ] };

    key  <AD04>          { [  p,          P           ] };
    key  <AD05>          { [  y,          Y           ] };
#+end_src

Upper row, right hand.

#+begin_src xkb :tangle keycodes/moonlander
    <AD06>          = 41;
    <AD07>          = 42;
    <AD08>          = 54;
    <AD09>          = 27;
    <AD10>          = 46;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <AD06>          { [  f,          F           ] };
    key  <AD07>          { [  g,          G           ] };
    key  <AD08>          { [  c,          C           ] };
    key  <AD09>          { [  r,          R           ] };
    key  <AD10>          { [  l,          L           ] };
#+end_src

Bottom row, left hand.

#+begin_src xkb :tangle keycodes/moonlander
    <AB01>          = 48;
    <AB02>          = 24;
    <AB03>          = 44;
    <AB04>          = 45;
    <AB05>          = 53;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <AB01>          { [  apostrophe, quotedbl    ] };
    key  <AB02>          { [  q,          Q           ] };
    key  <AB03>          { [  j,          J           ] };
    key  <AB04>          { [  k,          K           ] };
    key  <AB05>          { [  x,          X           ] };
#+end_src

Bottom row, right hand.

#+begin_src xkb :tangle keycodes/moonlander
    <AB06>          = 56;
    <AB07>          = 58;
    <AB08>          = 25;
    <AB09>          = 55;
    <AB10>          = 52;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <AB06>          { [  b,          B           ] };
    key  <AB07>          { [  m,          M           ] };
    key  <AB08>          { [  w,          W           ] };
    key  <AB09>          { [  v,          V           ] };
    key  <AB10>          { [  z,          Z           ] };
#+end_src

** Symbols and Numbers

I decided to reserve the top-left key ~Esc~ and the top-right key for ~Backspace~.  This means that we won't be able to fit the pound key into the top row, but that's fine.

The top row keys are a bit more complicated because the *Moonlander* sends =Shift + Key= whenever that key only exists in shifted state on a standard PC keyboard with the ~QWERTY~ layout.  We will distinguish between =normal= and =shifted= state in the following tables.

Once we go into our shifted layer, we will come across the situation that we will receive a keycode that has not already been used our regular layer.  We need to introduce new symbols for those.

Top row, left hand:

#+begin_src xkb :tangle keycodes/moonlander
    <AE00>          = 13;  // shifted
    <AE01>          = 16;  // shifted
    <AE02>          = 34;  // normal
    // 34           -> shifted AE02
    <AE03>          = 35;  // shifted
    <AE04>          = 18;  // shifted
#+end_src

Top row, right hand:

#+begin_src xkb :tangle keycodes/moonlander
    <AE05>          = 21;  // regular
    <AE06>          = 17;  // shifted
    <AE07>          = 19;  // shifted
    // 21           -> shifted AE05
    // 35           -> regular AE03
    <AE08>          = 10;  // shifted
#+end_src

Shift layer, left hand:

#+begin_src xkb :tangle keycodes/moonlander
    <AE09>          = 49;  // shifted
    <AE10>          = 14;  // shifted
    // 16           -> regular AE01
    // 14           -> regular AE10
    <AE11>          = 12;  // regular
    // 10           -> regular AE08
#+end_src

Shift layer, right hand:

#+begin_src xkb :tangle keycodes/moonlander
    // 18           -> regular AE04
    // 19           -> regular AE07
    <AE12>          = 11;  // regular
    // 13           -> regular AE00
    <AE13>          = 15;  // regular
    // 17           -> regular AE06
#+end_src

This is =ampersand= / =grave=, placed on the top right below =backspace=.

#+begin_src xkb :tangle keycodes/moonlander
    // 12           -> shifted AE11
    // 49           -> regular AE09
#+end_src

I have =at= / =asciicircum= at the bottom right, we include it here became it maps onto the two "missing" codes from this table.

#+begin_src xkb :tangle keycodes/moonlander
    // 11           -> shifted AE12
    // 15           -> shifted AE13
#+end_src

Alright, now that we got all the keycodes, we can do the symbol mapping.

#+begin_src xkb :tangle symbols/moonlander
    key  <AE00>          { [  4,              dollar      ] };
    key  <AE01>          { [  7,              ampersand   ] };
    key  <AE02>          { [  bracketleft,    braceleft   ] };
    key  <AE03>          { [  bracketright,   braceright  ] };
    key  <AE04>          { [  9,              parenleft   ] };
    key  <AE05>          { [  equal,          plus        ] };
    key  <AE06>          { [  8,              asterisk    ] };
    key  <AE07>          { [  0,              parenright  ] };
    key  <AE08>          { [  1,              exclam      ] };
    key  <AE09>          { [  grave,          asciitilde  ] };
    key  <AE10>          { [  5,              percent     ] };
    key  <AE11>          { [  3,              numbersign  ] };
    key  <AE12>          { [  2,              at          ] };
    key  <AE13>          { [  6,              asciicircum  ] };
#+end_src

** Symbols - continued

Alright, now let's move on to the =slash=, =at= and =backslash=  keys.  Their location is not final yet, especially not for =slash=.  I've placed them at the bottom right for the moment.  Since we've already done =at= in the symbols section, we need to skip that.

#+begin_src xkb :tangle keycodes/moonlander
    <AD11>          = 61;
    <BKSL>          = 51;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <AD11>          { [  slash,      question    ] };
    key  <BKSL>          { [  backslash,  bar         ] };
#+end_src

I have =minus= and =underscore= in the bottom right - their position isn't final either.

#+begin_src xkb :tangle keycodes/moonlander
    <AD12>          = 20;  // regular
    // 20           -> shifted AD12
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <AD12>          { [  minus,      underscore      ] };
#+end_src

** Inner quadrant

In the inner quadrant, I have the =Home=, =End=, =PgUp= and =PgDn= keys.

#+begin_src xkb :tangle keycodes/moonlander
    <HOME>          = 97;
    <END>           = 103;
    <PGUP>          = 99;
    <PGDN>          = 105;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <HOME>          { [  Home      ] };
    key  <END>           { [  End       ] };
    key  <PGUP>          { [  Prior     ] };
    key  <PGDN>          { [  Next      ] };
#+end_src


* Symbol Layer

We can finally move on to the symbol layer!

** Arrow Keys

#+begin_src xkb :tangle keycodes/moonlander
    <UP>            = 98;
    <DOWN>          = 104;
    <LEFT>          = 100;
    <RGHT>          = 102;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <UP>            { [  Up      ] };
    key  <DOWN>          { [  Down    ] };
    key  <LEFT>          { [  Left    ] };
    key  <RGHT>          { [  Right   ] };
#+end_src

** Arithmetic Keys

#+begin_src xkb :tangle keycodes/moonlander
    <KPAD>          = 86;
    <KPSU>          = 82;
    <KPMU>          = 63;
    <KPDV>          = 112;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <KPAD>          { [  KP_Add        ] };
    key  <KPMU>          { [  KP_Multiply   ] };
    key  <KPSU>          { [  KP_Subtract   ] };
    key  <KPDV>          { [  KP_Divide     ] };
#+end_src

** Numpad Numbers

There is actually a difference between the number keys in the top row and those in the number pad.  They have distinct key codes - and the Windows login screen apparently only accepts the numpad version when using a PIN with a smart card.  I had these previously bound to the "regular" numbers, but was having problems logging into Windows until I replaced them with the numpad versions.

#+begin_src xkb :tangle keycodes/moonlander
    <KP7>           = 79;
    <KP8>           = 80;
    <KP9>           = 81;
    <KP4>           = 83;
    <KP5>           = 84;
    <KP6>           = 85;
    <KP1>           = 87;
    <KP2>           = 88;
    <KP3>           = 89;
    <KP0>           = 90;
#+end_src


#+begin_src xkb :tangle symbols/moonlander
    key  <KP0>           { [  KP_0  ] };
    key  <KP1>           { [  KP_1  ] };
    key  <KP2>           { [  KP_2  ] };
    key  <KP3>           { [  KP_3  ] };
    key  <KP4>           { [  KP_4  ] };
    key  <KP5>           { [  KP_5  ] };
    key  <KP6>           { [  KP_6  ] };
    key  <KP7>           { [  KP_7  ] };
    key  <KP8>           { [  KP_8  ] };
    key  <KP9>           { [  KP_9  ] };
#+end_src

** Function keys

#+begin_src xkb :tangle keycodes/moonlander
    <FK01>          = 67;
    <FK02>          = 68;
    <FK03>          = 69;
    <FK04>          = 70;
    <FK05>          = 71;
    <FK06>          = 72;
    <FK07>          = 73;
    <FK08>          = 74;
    <FK09>          = 75;
    <FK10>          = 76;
    <FK11>          = 95;
    <FK12>          = 96;
#+end_src

#+begin_src xkb :tangle symbols/moonlander
    key  <FK01>          { [  F1    ] };
    key  <FK02>          { [  F2    ] };
    key  <FK03>          { [  F3    ] };
    key  <FK04>          { [  F4    ] };
    key  <FK05>          { [  F5    ] };
    key  <FK06>          { [  F6    ] };
    key  <FK07>          { [  F7    ] };
    key  <FK08>          { [  F8    ] };
    key  <FK09>          { [  F9    ] };
    key  <FK10>          { [  F10   ] };
    key  <FK11>          { [  F11   ] };
    key  <FK12>          { [  F12   ] };
#+end_src

* Footer

Finally, close both files.

#+begin_src xkb :tangle keycodes/moonlander
};
#+end_src

#+begin_src xkb :tangle symbols/moonlander
};
#+end_src

That's it.
