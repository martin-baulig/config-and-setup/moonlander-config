# [[file:README.org::*Makefile][Makefile:1]]
# Using GNU Make Syntax
DESTDIR			= /usr/X11R6/share/X11
XKB_DESTDIR		= $(DESTDIR)/xkb
CONF_DESTDIR	= $(DESTDIR)/xorg.conf.d
# Makefile:1 ends here

# [[file:README.org::*Makefile][Makefile:2]]
XKB_FILES		= rules/moonlander rules/moonlander.lst \
				  keycodes/moonlander symbols/moonlander

CONF_FILES		= 80-keyboard.conf
# Makefile:2 ends here

# [[file:README.org::*Makefile][Makefile:3]]
DIFF			= diff
DOAS			= doas
INSTALL			= install
INSTALL_ARGS	= -m 0644 -o root -g bin
# Makefile:3 ends here

# [[file:README.org::*Makefile][Makefile:4]]
.PHONY:			diff install update

define execute-command
$(1)

endef
# Makefile:4 ends here

# [[file:README.org::*Makefile][Makefile:5]]
diff::
	$(foreach x,$(CONF_FILES),$(call execute-command,$(DIFF) -du $(CONF_DESTDIR)/$(x) $(x)))
	$(foreach x,$(XKB_FILES),$(call execute-command,$(DIFF) -du $(XKB_DESTDIR)/$(x) $(x)))
# Makefile:5 ends here

# [[file:README.org::*Makefile][Makefile:6]]
install::
	$(foreach x,$(CONF_FILES),$(call execute-command,$(DOAS) $(INSTALL) $(INSTALL_ARGS) $(x) $(CONF_DESTDIR)/$(x)))
	$(foreach x,$(XKB_FILES),$(call execute-command,$(DOAS) $(INSTALL) $(INSTALL_ARGS) $(x) $(XKB_DESTDIR)/$(x)))
# Makefile:6 ends here

# [[file:README.org::*Makefile][Makefile:7]]
setup::
	setxkbmap -I $(CURDIR) -rules moonlander -model moonlander -option "" -verbose 10
# Makefile:7 ends here
