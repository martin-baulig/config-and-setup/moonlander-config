;;; grammar-wy.el --- Generated parser support file

;; Copyright (C) 2023 Martin Baulig

;; Author: Martin Baulig <martin@baulig.is>
;; Created: 2023-04-09 19:44:07-0400
;; Keywords: syntax
;; X-RCS: $Id$

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This software is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; PLEASE DO NOT MANUALLY EDIT THIS FILE!  It is automatically
;; generated from the grammar file grammar.wy.

;;; Code:

(require 'semantic/lex)
(require 'semantic/wisent)

;;; Prologue
;;

;;; Declarations
;;
(eval-and-compile (defconst grammar-wy--expected-conflicts
                    nil
                    "The number of expected shift/reduce conflicts in this grammar."))

(defconst grammar-wy--keyword-table
  (semantic-lex-make-keyword-table 'nil 'nil)
  "Table of language keywords.")

(defconst grammar-wy--token-table
  (semantic-lex-make-type-table 'nil 'nil)
  "Table of lexical tokens.")

(defconst grammar-wy--parse-table
  (wisent-compiled-grammar
   (nil nil)
   (statement statement))
  "Parser table.")

(defun grammar-wy--install-parser ()
  "Setup the Semantic Parser."
  (semantic-install-function-overrides
   '((semantic-parse-stream . wisent-parse-stream)))
  (setq semantic-parser-name "LALR"
        semantic--parse-table grammar-wy--parse-table
        semantic-debug-parser-source "grammar.wy"
        semantic-flex-keywords-obarray grammar-wy--keyword-table
        semantic-lex-types-obarray grammar-wy--token-table)
  ;; Collect unmatched syntax lexical tokens
  (add-hook 'wisent-discarding-token-functions
            #'wisent-collect-unmatched-syntax nil t))


;;; Analyzers
;;

;;; Epilogue
;;

(provide 'grammar-wy)

;; Local Variables:
;; version-control: never
;; no-update-autoloads: t
;; End:

;;; grammar-wy.el ends here
